# Create virtualenv
python3 -m venv venv
source venv/bin/activate

# Install dependencies
pip install -r requirements.txt

# Run the app
python3 -m uvicorn transcription-api.main:app --reload

# Test the endpoint
POST /api/transcription
{
    "file": [some_file]
}