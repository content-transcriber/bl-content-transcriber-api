from fastapi import FastAPI, UploadFile, File, Form
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import StreamingResponse
from .utils.constants import SUPPORTED_EXTENSIONS, SUPPORTED_OUTPUT_FORMATS, SUPPORTED_TASKS
from .transcribers.Transcriber import Transcriber

app = FastAPI()

# Configure CORS to allow all origins
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post("/api/transcription")
async def transcribe_text(
        file: UploadFile = File(...),
        output_format: str = Form(...),
        task: str = Form(...)
):
    # Read the contents of the file
    file_contents = await file.read()

    # Get the file format
    file_format = file.filename.split(".")[-1]
    if file_format not in SUPPORTED_EXTENSIONS:
        raise ValueError("Unsupported file format: {}".format(file.filename))

    # Validate output_format
    if output_format not in SUPPORTED_OUTPUT_FORMATS:
        raise ValueError("Invalid output format: {}. Only 'srt' and 'txt' are accepted.".format(output_format))

    # Validate task
    if task not in SUPPORTED_TASKS:
        raise ValueError("Invalid task: {}. Only 'translate' and 'transcribe' are accepted.".format(task))

    transcriber = Transcriber()

    transcription = await transcriber.transcribe(
        file_contents, file_format, "whisper", output_format, task
    )

    # Return a StreamingResponse with the contents of the buffer as the response body
    return StreamingResponse(transcription, media_type="text/srt")
