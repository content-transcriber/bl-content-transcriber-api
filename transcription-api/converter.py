from pydub import AudioSegment
from .utils.constants import SUPPORTED_EXTENSIONS
import time
from io import BytesIO

# Define a function to convert MP4 to MP3
def convert_video_to_mp3(filename, file_bytes):

    # Extract file extension from input file and lower case
    extension = filename.split(".")[-1].lower()

    file = BytesIO(file_bytes)

    # Check if the file is in supported format
    if extension in SUPPORTED_EXTENSIONS:
        # Load the MP4 file
        mp4_audio = AudioSegment.from_file(file, format=extension)
        
        # export mp4_audio to mp3 and save it in memory
        mp3_audio = mp4_audio.export(format="mp3", out_f=BytesIO())

        return mp3_audio
    else:
        raise ValueError("Input file must be of one of the following formats: {}".format(SUPPORTED_EXTENSIONS))