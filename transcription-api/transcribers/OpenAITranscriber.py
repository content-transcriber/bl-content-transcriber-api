import io
from ..utils.files import cut_audio, join_srt, remove_directory, create_directory
import openai
import os
import time
import concurrent.futures

class OpenAITranscriber:

    # The maximum size of a file that can be transcribed by the Whisper API
    MAX_FILE_SIZE = 25 # MB
    openai.api_key = "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"

    async def transcribe(self, file_contents: bytes, file_format: str, output_format: str, task: str):

        file_chunks = await cut_audio(file_contents, file_format, OpenAITranscriber.MAX_FILE_SIZE)

        transcription_chunks = self.transcribe_chunks(file_chunks, output_format, task)

        srt_buffer = io.BytesIO()

        srt_buffer.write(transcription_chunks.encode())
        srt_buffer.seek(0)

        return srt_buffer

    def transcribe_chunks(self, file_chunks, output_format, task):
        # Get the current date and time as a string
        dir_path = str(int(time.time()))

        create_directory(dir_path)

        transcripts = [None] * len(file_chunks)
        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = []
            for i, chunk in enumerate(file_chunks):
                # Write the chunk to disk
                filename = os.path.join(dir_path, str(i) + ".mp3")
                with open(filename, "wb") as f:
                    f.write(chunk)
                audio_file= open(filename, "rb")
                futures.append(executor.submit(self.transcribe_chunk, audio_file, i, output_format, task))
            
            for future in concurrent.futures.as_completed(futures):
                i, transcript = future.result()
                transcripts[i] = transcript

        if(output_format == "srt"):
            # Join the SRT transcriptions and update the time codes
            joined_transcript = join_srt(transcripts)
        else:
            joined_transcript = "".join(transcripts)

        # remove the created directory dir_path and all it's content
        remove_directory(dir_path)

        return joined_transcript
    
    def transcribe_chunk(self, audio_file, i, output_format, task):
        params = {
            "response_format": output_format
        }
        if(task == "transcribe"):
            transcript = openai.Audio.transcribe("whisper-1", audio_file, **params)
        else:
            transcript = openai.Audio.translate("whisper-1", audio_file, **params)
        
        return i, transcript
