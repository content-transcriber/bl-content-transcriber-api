from ..utils.times import to_delta_time
import whisper
import time
import io
from ..utils.files import remove_directory, create_directory

class WhisperTranscriber:

    async def transcribe(self, file_contents: bytes, file_format: str, output_format: str, task: str):
        # Get the current date and time as a string
        timestamp = str(int(time.time()))

        create_directory(timestamp)

        try:
            # save file_contents to a temporary file
            file_path = "{}/input_file.{}".format(timestamp, file_format)

            model = whisper.load_model("base")

            with open(file_path, "wb") as f:
                f.write(file_contents)

            result = model.transcribe(file_path, task = task)

            if(output_format == "srt"):
                return self.build_srt_transcription(result)
            else:
                return self.build_text_transcription(result)
            
        except Exception as e:
            print(e)
            raise Exception("Something went wrong...")
        finally:
            # remove the created directory dir_path and all it's content
            remove_directory(timestamp)

    def build_srt_transcription(self, result):
        keys = ["start", "end", "text"]
        mappedSegments = [{key: s[key] for key in keys} for s in result["segments"]]
        result = { "segments": mappedSegments, "language": result["language"] }
    
        # Create a buffer to store the contents of the .srt file as bytes
        srt_buffer = io.BytesIO()

        for i, transcription in enumerate(result["segments"]):
            start_time = transcription["start"]
            end_time = transcription["end"]
            text = transcription["text"]

            srt_buffer.write(f"{i+1}\n".encode())
            srt_buffer.write(f"{to_delta_time(start_time)} --> {to_delta_time(end_time)}\n".encode())
            srt_buffer.write(f"{text}\n\n".encode())

        # Set the buffer position to the beginning of the buffer
        srt_buffer.seek(0)
        return srt_buffer
    
    def build_text_transcription(self, result):
        srt_buffer = io.BytesIO()
        srt_buffer.write(result["text"].encode())

        # Set the buffer position to the beginning of the buffer
        srt_buffer.seek(0)
        return srt_buffer
        
