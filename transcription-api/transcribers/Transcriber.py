from .GladiaTranscriber import GladiaTranscriber
from .OpenAITranscriber import OpenAITranscriber
from .WhisperTranscriber import WhisperTranscriber

class Transcriber:
    def __init__(self):
        self.transcribers = {
            "openai": OpenAITranscriber(),
            "gladia": GladiaTranscriber(),
            "whisper": WhisperTranscriber()
        }

    async def transcribe(self, file_contents: bytes, file_format: str, method: str, output_format: str, task: str):
        transcriber = self.transcribers.get(method)
        if transcriber is None:
            raise ValueError("Invalid format: {}".format(format))
    
        return await transcriber.transcribe(file_contents, file_format, output_format, task)
