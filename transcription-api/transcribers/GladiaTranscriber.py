import io
import time
import requests
from ..utils.files import create_directory, remove_directory

class GladiaTranscriber:

    FORMATS = {
        "mp3": "audio/mpeg",
        "wav": "audio/wav",
        "mp4": "video/mp4",
    }

    API_KEY = "baa89a8b-7c3c-4c8e-8b1e-3c5c253f9565"
    BASE_URL = "https://api.gladia.io/audio/text"

    async def transcribe(self, file_contents: bytes, file_format: str, output_format: str, task: str):

        dir_path = str(int(time.time()))
        create_directory(dir_path)

        try:
            # save file_contents to a temporary file
            file_path = "{}/input_file.{}".format(dir_path, file_format)

            with open(file_path, "wb") as f:
                f.write(file_contents)

            # Call Gladia API
            transcription = await self.call_gladia_api(file_path, file_format, output_format, task)

            srt_buffer = io.BytesIO()

            srt_buffer.write(transcription.encode())
            srt_buffer.seek(0)

            return srt_buffer
            
        except Exception as e:
            print(e)
            raise Exception("Something went wrong...")
        finally:
            # remove the created directory dir_path and all it"s content
            remove_directory(dir_path)

    async def call_gladia_api(self, file_path: str, file_format: str, response_format: str, task: str):
        headers = {
                "accept": "application/json",
                "x-gladia-key": self.API_KEY,
                # requests won"t add a boundary if this header is set when you pass files=
                # "Content-Type": "multipart/form-data",
            }

        if(response_format == "srt"):
            output_format = "srt"
        else:
            output_format = "json"
        
        if(task == 'transcribe'):
            api_uri = "/audio-transcription/"
        elif(task == 'translate'):
            api_uri = "/audio-translated-transcription/"

        files = {
            "audio": (file_path, open(file_path, "rb"), self.FORMATS[file_format]),
            "language": (None, "english"),
            "language_behaviour": (None, "automatic single language"),
            "output_format": (None, output_format)
        }

        response = requests.post(self.BASE_URL + api_uri, headers=headers, files=files)
        
        response = response.json()["prediction"]
        return response
        