SUPPORTED_EXTENSIONS = ["mp4", "wav", "mp3"]
SUPPORTED_OUTPUT_FORMATS = ["srt", "txt"]
SUPPORTED_TASKS = ["translate", "transcribe"]