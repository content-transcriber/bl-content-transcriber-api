from datetime import datetime, timedelta

def to_delta_time(time: float) -> str:
    # Converts a time in seconds to a string in the format HH:MM:SS,mmm
    delta = timedelta(seconds=time)
    dt = datetime(1, 1, 1) + delta
    return dt.strftime("%H:%M:%S,%f")[:-3]




