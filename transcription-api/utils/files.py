from io import BytesIO
import shutil
from pydub import AudioSegment
import io
import pysrt
import os

async def cut_audio(file_contents: bytes, file_format: str, chunk_size: int):
    # Load the audio file into memory using Pydub
    audio_file = AudioSegment.from_file(BytesIO(file_contents), format=file_format)

    # Convert the chunk size from MB to bytes
    chunk_size_bytes = chunk_size * 1024 * 1024

    # get total length of audio file in milliseconds
    total_length_ms = len(audio_file)

    # convert chunk size to milliseconds
    chunk_size_ms = chunk_size_bytes * 8 / (audio_file.frame_rate * audio_file.sample_width * audio_file.channels) * 1000

    # calculate number of chunks needed
    num_chunks = int(total_length_ms // chunk_size_ms) + 1

    audio_chunks = []
    # split audio file into chunks and save to output directory
    for i in range(num_chunks):
        start_time_ms = i * chunk_size_ms
        end_time_ms = (i + 1) * chunk_size_ms
        if end_time_ms > total_length_ms:
            end_time_ms = total_length_ms
        chunk = audio_file[start_time_ms:end_time_ms]
        buffer = io.BytesIO()
        chunk.export(buffer, format="mp3")
        audio_chunks.append(buffer.getvalue())

    return audio_chunks

def join_srt(subtitles_array):
    merged_subs = pysrt.SubRipFile()
    for sub in subtitles_array:
        s = pysrt.SubRipFile.from_string(sub)
        if merged_subs:
            last_end_time = merged_subs[-1].end.to_time()
            for item in s:
                item.start.shift(hours=last_end_time.hour,
                                 minutes=last_end_time.minute,
                                 seconds=last_end_time.second,
                                 milliseconds=last_end_time.microsecond // 1000)
                item.end.shift(hours=last_end_time.hour,
                               minutes=last_end_time.minute,
                               seconds=last_end_time.second,
                               milliseconds=last_end_time.microsecond // 1000)
        merged_subs.extend(s)

    for i, item in enumerate(merged_subs):
        item.index = i + 1

    items_str = [str(item) for item in merged_subs]

    return '\n'.join(items_str)

def remove_directory(directory_path):
    shutil.rmtree(directory_path) 

def create_directory(directory_path):
    dir_path = os.path.join(os.getcwd(), directory_path)
    os.makedirs(dir_path)